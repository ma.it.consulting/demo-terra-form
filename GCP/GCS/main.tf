resource "google_storage_bucket" "demo_bucket" {
  name          = "tf-mohamed-neosoft"
  location      = "US"
  
  lifecycle_rule {
    condition {
      age = 5
    }
    action {
      type = "SetStorageClass"
      storage_class = "COLDLINE"
    }
  }

  retention_policy{
    is_locked = true
    retention_period = 259200  
  }

}

resource "google_storage_bucket_object" "picture" {
  name   = "butterfly01"
  source = "papillon.jpeg"
  bucket = google_storage_bucket.demo_bucket.name
}

