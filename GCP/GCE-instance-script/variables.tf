variable "project" {
type=string  
default = "nifty-catfish-370607"

}
variable "instance_name" {
type=string  
default = "instance-script"

}

variable "region" {
  type = string
  default = "us-central1"
}

variable "zone" {
type=string 
default = "us-central1-a"
}


variable "network_tags" {
  type=list(string)
  default = [ "http-server" ]
}

variable "machine_type" {
type = map
default = {
    dev = "e2-medium"
    test = "e2-small"
    prod = "e2-micro"
}  
}
variable "environement" {
    type = string
    default = "dev"
  
}

variable "instance-imagee" {
type=string
default = "debian-cloud/debian-11"  
}


