resource "google_compute_instance" "default" {
  name         = var.instance_name
  machine_type = var.machine_type[var.environement]
  zone         = var.zone

  tags = var.network_tags

  boot_disk {
    initialize_params {
      image = var.instance-imagee
      
    }
  }

  network_interface {
    network = "default"
    access_config {
     
    }
  }


  metadata_startup_script = "./startup-script"

  
}