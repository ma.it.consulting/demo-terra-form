terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.46.0"
    }
  }
}

provider "google" {
    project = "nifty-catfish-370607"
    region  = "us-central1"
    zone = "us-central1-a"
}


resource "google_storage_bucket" "GCS1" {
  name = "first-bucket-terraform-mohamed"
  location = "US"
}