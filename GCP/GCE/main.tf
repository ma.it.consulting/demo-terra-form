resource "google_compute_instance" "instance1" {
  name         = var.name_instance
  machine_type = var.machine
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

  network_interface {
    network = "default"
  }
}

resource "random_string" "name" {
  length = 25
  min_numeric = 10
}

resource "google_compute_disk" "disk1" {
  name  = "${var.machine}-${random_string.name.result}"  
  type  = "pd-ssd"
  zone  = var.zone
  image = "debian-11-bullseye-v20220719"
  size = 15 
}

resource "google_compute_attached_disk" "attache1" {
  disk     = google_compute_disk.disk1.id
  instance = google_compute_instance.instance1.id
}
