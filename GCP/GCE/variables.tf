variable "name_instance" {
type=string
default = "mohamed-instance-terra"
}

variable "machine" {
type=string
default = "e2-medium"
}

variable "zone" {
type=string
default = "us-central1-a"
}