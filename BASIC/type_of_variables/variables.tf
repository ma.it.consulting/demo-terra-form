variable "filename1" {
  type = string
    default = "file2.txt"
}

/* variable "content1" {
type = number
default = 45  
} */

/* variable "content1" {
type = bool
default = true  
} */ 

/* variable "content1" {
type = list(string)
default = ["coucou","on est en final","cool"]
}  */

/* variable "content1" {
type = bool
default = true  
}  */

variable "content1" {
type = tuple([string, bool,number])
default = ["coucou",false,67]
} 