resource "random_integer" "rint" {
min = 50
max = 100

lifecycle {
    # create_before_destroy = true
    #prevent_destroy = true
    ignore_changes = [min]
  }
}

resource "random_string" "rstring" {
length = 15
min_numeric = 5
}


output "name1" {
value = random_integer.rint.result
}

output "name2" {
value = random_string.rstring.result
}