variable "filename1" {
    type = string
    default = "file.txt" 
}

variable "content1" {
    type = string
    default = "J'adore Terraform"
}